import { Fichier } from './fichier';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AllpdfService {
  fichier = {
    name: 'string',
    size: 'integer',
    lastModified: 'integer',
    avatar: { type: 'string', ipsum: 'small image' }
  } ;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }
  // recupere la list des fichiers
  public getFichiers(): Observable<Fichier[]> {
    return this.http.get<Fichier[]>(environment.backUrl + '/fichiers' );
  }

}
