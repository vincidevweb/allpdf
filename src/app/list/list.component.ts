import { AllpdfService } from './../allpdf.service';
import { Component, OnInit } from '@angular/core';
import { Fichier } from '../fichier';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  fichiers: Fichier[];
  selectedFiles: Fichier[];

  constructor(private service: AllpdfService) {
    this.service.getFichiers().subscribe(result => this.fichiers = result);
   }

  ngOnInit() {
  }

}
