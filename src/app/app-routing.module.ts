import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UploadComponent } from './upload/upload.component';
import { MergeComponent } from './merge/merge.component';
import { InsertComponent } from './insert/insert.component';
import { ManageComponent } from './manage/manage.component';
import { CutComponent } from './cut/cut.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'upload', component: UploadComponent },
  { path: 'manage', component: ManageComponent },
  { path: 'manage/merge', component: MergeComponent },  
  { path: 'manage/insert', component: InsertComponent },
  { path: 'manage/cut', component: CutComponent },
  { path: 'manage/delete', component: DeleteComponent },
  { path: 'manage/list', component: ListComponent },

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: HomeComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
