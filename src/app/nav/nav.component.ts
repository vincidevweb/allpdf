import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  private items: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items = [
        {
            label: 'home',
            routerLink: ['/home']
        },
        {
            label: 'upload',
            routerLink: ['/upload']
        },
        {
          label: 'manage',
          icon: 'pi pi-fw pi-cog',
          items: [
              {label: 'Merge', icon: 'pi pi-fw pi-sign-out', routerLink: ['/manage/merge']},
              {label: 'Insert', icon: 'pi pi-fw pi-sign-in', routerLink: ['/manage/insert']},
              {label: 'Cut', icon: 'pi pi-fw pi-clone', routerLink: ['/manage/cut']},
              {label: 'Delete', icon: 'pi pi-fw pi-trash', routerLink: ['/manage/delete']},
              {label: 'List', icon: 'pi pi-fw pi-list',routerLink: ['/manage/list']}
          ]
      }
    ];
}

}
