import { TestBed } from '@angular/core/testing';

import { AllpdfService } from './allpdf.service';

describe('AllpdfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllpdfService = TestBed.get(AllpdfService);
    expect(service).toBeTruthy();
  });
});
